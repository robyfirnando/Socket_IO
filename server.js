var express     = require('express');
var app         = express();
var server      = require('http').createServer(app);
var io          = require('socket.io').listen(server);




users = [];
connections = [];


console.log('server listenning...');

server.listen(process.env.PORT || 3000);

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

io.sockets.on('connection', function(socket){
    connections.push(socket);
    console.log('connected: %s socket connected',connections.length);
    
    //disconnect
    socket.on('disconnect', function(data){
        // if(!socket.username) return; 
        users.splice(users.indexOf(socket.username), 1);
        Updateusernames();
        connections.splice(connections.indexOf(socket), 1);
        console.log('disconnected: %s socket connected',connections.length);
    });

    //send messages
    socket.on('send message', function(data){
        console.log(data);
        io.sockets.emit('new message', {msg:data,user: socket.username});
    });

    //new users
    socket.on('new user', function(data,callback){
        callback(true);
        console.log(data);
        socket.username = data;
        users.push(socket.username);
        Updateusernames();
    });

    function Updateusernames(){
        io.sockets.emit('get users', users);
    }

    //event typing
    socket.on('typingMsg', function(data){
        io.sockets.emit('new typing', {pesan_ngetik:data,user: socket.username});
    });





    
});